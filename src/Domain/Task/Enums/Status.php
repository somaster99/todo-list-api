<?php

declare(strict_types=1);

namespace Domain\Task\Enums;

use Domain\Shared\Traits\EnumToArray;

enum Status: string
{
    use EnumToArray;

    case TODO = 'todo';

    case DONE = 'done';

}
