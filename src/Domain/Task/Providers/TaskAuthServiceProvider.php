<?php

declare(strict_types=1);

namespace Domain\Task\Providers;

use Domain\Task\Models\Task;
use Domain\Task\Policies\TaskPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class TaskAuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        Task::class => TaskPolicy::class,
    ];

    public function boot(): void
    {
        $this->registerPolicies();

    }
}
