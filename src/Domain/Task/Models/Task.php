<?php

declare(strict_types=1);

namespace Domain\Task\Models;

use App\Models\User;
use Domain\Task\Enums\Status;
use Illuminate\Support\Collection;
use Domain\Shared\Models\BaseModel;
use Domain\Shared\Traits\Searchable;
use Domain\Task\Builders\TaskBuilder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Task extends BaseModel
{
    use Searchable;

    protected $fillable = [
        'status',
        'priority',
        'title',
        'description',
        'completed_at',
        'parent_id',
    ];

    protected $hidden = [
        'deleted_at'
    ];

    public static array $sortables = [
        'id',
        'created_at',
        'completed_at',
        'priority'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected array $searchable = [
        'tasks.title'
    ];

    protected $casts = [
        'status' => Status::class
    ];

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'parent_id');
    }

    public function subTasks(): HasMany
    {
        /*
         * Using a request helper is not a good practice there.
         */
        return $this->tasks()->with('subTasks')
            ->whereStatus(request()->input('status'))
            ->wherePriority(request()->input('priority'))
            ->orderBy(request()->input('sort_by', 'id'), request()->input('order_by', 'ASC'));
    }

    public function parentTask(): BelongsTo
    {
        return $this->belongsTo(Task::class, 'parent_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function newEloquentBuilder($query): TaskBuilder
    {
        return new TaskBuilder($query);
    }

    public function hasUncompletedSubTasks(Task $task): bool
    {
        foreach ($task->subTasks as $subTask) {
            if (!($subTask->status === Status::DONE) || $this->hasUncompletedSubTasks($subTask)) {
                return true;
            }
        }

        return false;
    }

    public function deleteRecursive(): void
    {
        $this->load('subTasks');

        foreach ($this->subTasks as $subTask) {
            $subTask->deleteRecursive();
        }

        $this->delete();
    }

    /**
     * Internal recursive call function
     *
     * @param Task $task
     * @return Collection
     */
    public function getSubTasks(Task $task): Collection
    {
        $tree = collect();

        foreach ($task->subTasks as $task) {
            $tree->push($task);

            $subTree = $task->getSubTasks($task);
            if ($subTree->isNotEmpty()) {
                $tree->push($subTree);
            }
        }

        return $tree;
    }
}
