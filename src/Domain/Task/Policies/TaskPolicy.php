<?php

declare(strict_types=1);

namespace Domain\Task\Policies;

use App\Models\User;
use Domain\Task\Models\Task;
use Domain\Task\Enums\Status;
use Symfony\Component\HttpFoundation\Response;

class TaskPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return (bool)$user;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Task $task): bool
    {
        return $user->id === $task->user_id;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): true
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Task $task): bool
    {
        return $user->id === $task->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Task $task): bool
    {
        if (!($user->id === $task->user_id)) {
            return false;
        }

        abort_if(
            boolean: $task->status === Status::DONE,
            code: Response::HTTP_BAD_REQUEST,
            message: 'The task already completed, can`t be deleted.'
        );

        return true;
    }

    public function markAsCompleted(User $user, Task $task): bool
    {
        if ($user->id !== $task->user_id) {
            return false;
        }

        abort_if(
            boolean: $task->hasUncompletedSubTasks($task),
            code: Response::HTTP_BAD_REQUEST,
            message: 'The task has uncompleted subtasks.'
        );

        return true;
    }
}
