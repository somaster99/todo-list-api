<?php

declare(strict_types=1);

namespace Domain\Task\DataTransferObjects;

use Spatie\LaravelData\Data;
use Domain\Task\Enums\Status;
use Spatie\LaravelData\Lazy;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\Attributes\Validation\Max;
use Spatie\LaravelData\Attributes\Validation\Enum;
use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\Attributes\Validation\Nullable;
use Spatie\LaravelData\Attributes\Validation\Required;
use Spatie\LaravelData\Attributes\Validation\StringType;
use Spatie\LaravelData\Attributes\Validation\IntegerType;
use Spatie\LaravelData\Attributes\Validation\DigitsBetween;

class TaskData extends Data
{
    public function __construct(
        #[Required, IntegerType]
        public readonly int $id,
        #[Required, StringType, Max(250)]
        public readonly string $title,
        #[Required, StringType, Enum(Status::class), Max(250)]
        public readonly string $status,
        #[Required, IntegerType, DigitsBetween(1, 5)]
        public readonly int $priority,
        #[Nullable, StringType, Max(65535)]
        public readonly ?string $description,
        #[Nullable, IntegerType]
        public readonly ?int $parent_id,
        public readonly ?string $completed_at,
        public readonly string $created_at,
        public readonly string $updated_at,
        #[Nullable, DataCollectionOf(TaskData::class)]
        public null|Lazy|DataCollection $sub_tasks,
    ) {
    }
}
