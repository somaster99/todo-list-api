<?php

declare(strict_types=1);

namespace Domain\Task\DataTransferObjects;

use Domain\Task\Models\Task;
use Spatie\LaravelData\Data;
use Domain\Task\Enums\Status;
use Spatie\LaravelData\Attributes\Validation\Max;
use Spatie\LaravelData\Attributes\Validation\Enum;
use Spatie\LaravelData\Attributes\Validation\Exists;
use Spatie\LaravelData\Attributes\Validation\Nullable;
use Spatie\LaravelData\Attributes\Validation\Required;
use Spatie\LaravelData\Attributes\Validation\StringType;
use Spatie\LaravelData\Attributes\Validation\IntegerType;
use Spatie\LaravelData\Attributes\Validation\DigitsBetween;

class UpsertTaskData extends Data
{
    public function __construct(
        #[Required, StringType, Max(250)]
        public readonly string $title,
        #[Required, StringType, Enum(Status::class)]
        public readonly string $status,
        #[Nullable, StringType, Max(65535)]
        public readonly string $description,
        #[Required, IntegerType, DigitsBetween(1, 5)]
        public readonly int $priority,
        #[Nullable, IntegerType, Exists(Task::class)]
        public readonly ?int $parent_id,
    ) {
    }
}
