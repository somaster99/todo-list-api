<?php

declare(strict_types=1);

namespace Domain\Task\DataTransferObjects;

use Spatie\LaravelData\Data;
use Domain\Task\Enums\Status;
use Spatie\LaravelData\Attributes\Validation\In;
use Spatie\LaravelData\Attributes\Validation\Max;
use Spatie\LaravelData\Attributes\Validation\Enum;
use Spatie\LaravelData\Attributes\Validation\Regex;
use Spatie\LaravelData\Attributes\Validation\Nullable;
use Spatie\LaravelData\Attributes\Validation\Sometimes;
use Spatie\LaravelData\Attributes\Validation\StringType;
use Spatie\LaravelData\Attributes\Validation\IntegerType;
use Spatie\LaravelData\Attributes\Validation\DigitsBetween;

class FilterTaskData extends Data
{
    public function __construct(
        #[Nullable, Sometimes, StringType, Max(250)]
        public readonly ?string $search,
        #[Nullable, Sometimes, StringType, Enum(Status::class), Max(250)]
        public readonly ?string $status,
        #[Nullable, Sometimes, IntegerType, DigitsBetween(1, 5)]
        public readonly ?int $priority,
        #[StringType, Max(250), In([
            'id',
            'created_at',
            'completed_at',
            'priority'
        ])]
        public readonly string $sort_by,
        #[StringType, Max(250), Regex('/^(?:asc|desc)$/i')]
        public readonly string $order_by,
    ) {
    }
}
