<?php

declare(strict_types=1);

namespace Domain\Task\Builders;

use Domain\Task\Enums\Status;
use Illuminate\Database\Eloquent\Builder;

class TaskBuilder extends Builder
{

    public function whereUser(?int $userId): self
    {
        return $userId ? $this->where('user_id', $userId) : $this;
    }

    /**
     * @param class-string<Status>|null $status
     * @return $this
     */
    public function whereStatus(?string $status): self
    {
        return $status ? $this->where('status', $status) : $this;
    }

    public function wherePriority(?int $priority): self
    {
        return $priority ? $this->where('priority', $priority) : $this;
    }

    public function whereTitle(?string $title): self
    {
        /*
        * Laravel full-text index use the natural language mode you need to type an exact full word.
        */
        return $title ? $this->whereFullText('title', $title) : $this;
    }
}
