<?php

declare(strict_types=1);

namespace Domain\Task\Actions;

use Domain\Task\Models\Task;
use Domain\Task\DataTransferObjects\UpsertTaskData;

readonly final class CreateTaskAction
{
    public function execute(UpsertTaskData $taskData): Task
    {
        return auth()->user()->tasks()->create($taskData->toArray());
    }
}
