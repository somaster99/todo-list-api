<?php

declare(strict_types=1);

namespace Domain\Task\Actions;

use Domain\Task\Models\Task;
use Domain\Task\DataTransferObjects\UpsertTaskData;

readonly final class UpdateTaskAction
{
    public function execute(Task $task, UpsertTaskData $upsertTaskData): Task
    {
        $task->update($upsertTaskData->toArray());
        return $task;
    }
}
