<?php

declare(strict_types=1);

namespace Domain\Task\Actions;

use Domain\Task\Models\Task;
use Domain\Task\Enums\Status;

readonly final class CompleteTaskAction
{
    public function execute(Task $task): Task
    {
        $task->update([
            'status' => Status::DONE->value,
            'completed_at' => now()
        ]);
        return $task;
    }
}
