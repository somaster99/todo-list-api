<?php

declare(strict_types=1);

namespace Domain\Task\Actions;

use Domain\Task\Models\Task;
use Domain\Task\DataTransferObjects\FilterTaskData;

readonly final class GetTasksAction
{
    public function execute(FilterTaskData $filterTaskData)
    {
        $userId = auth()->user()->id;

        return Task::query()
            ->with('subTasks')
            ->whereUser($userId)
            ->search($filterTaskData->search)
            ->whereStatus($filterTaskData->status)
            ->wherePriority($filterTaskData->priority)
            ->orderBy($filterTaskData->sort_by, $filterTaskData->order_by)
            ->get();
    }
}
