<?php

declare(strict_types=1);

namespace Domain\Task\Actions;

use Domain\Task\Models\Task;
use Illuminate\Support\Facades\DB;

class DeleteTaskAction
{
    public function execute(Task $task): void
    {
        DB::transaction(function () use ($task) {
            $task->deleteRecursive();
            $task->subTasks()->delete();
        }, 2);
    }
}
