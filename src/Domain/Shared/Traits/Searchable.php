<?php

declare(strict_types=1);

namespace Domain\Shared\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Searchable
{
    /**
     *
     * @param string $term
     * @return string
     */
    protected function fullTextWildcards(string $term): string
    {
        $reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
        $term = str_replace($reservedSymbols, '', $term);
        $words = explode(' ', $term);
        foreach ($words as $key => $word) {
            if (strlen($word) >= 3) {
                $words[$key] = '+' . $word . '*';
            }
        }

        return implode(' ', $words);
    }

    /**
     *
     * @param Builder $query
     * @param string|null $term
     * @return Builder
     */
    public function scopeSearch(Builder $query, ?string $term = null): Builder
    {
        if ($term) {
            $columns = implode(',', $this->searchable);
            $query->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($term));
        }

        return $query;
    }
}
