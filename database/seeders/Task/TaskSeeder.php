<?php

declare(strict_types=1);

namespace Database\Seeders\Task;

use App\Models\User;
use Carbon\Carbon;
use Domain\Task\Models\Task;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::all();

        foreach ($users as $user) {
            $this->createNestedTask($user);
        }
    }

    private function createNestedTask(User $user, int $level = 1, ?Task $parentTask = null): void
    {
        $taskFactory = Task::factory()->make();
        $taskFactory->parent_id = $parentTask?->id;
        $taskFactory->completed_at = $level % 3 === 0
            ? Carbon::parse($taskFactory->created_at)->addMonths(rand(1, 12))
            : null;
        $task = $user->tasks()->create($taskFactory->toArray());

        if ($level < 8) {
            $subtaskCount = rand(0, 7);
            for ($i = 0; $i < $subtaskCount; $i++) {
                $this->createNestedTask($user, $level + 1, $task);
            }
        }
    }
}
