<?php

declare(strict_types=1);

namespace Database\Seeders\User;

use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /*
         * Default user
         */
        User::factory()->create([
            'name' => 'test',
            'email' => 'test@example.com'
        ]);

        User::factory()->count(10)->create();
    }
}
