<?php

declare(strict_types=1);

use App\Models\User;
use Domain\Task\Enums\Status;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class)
                ->constrained()
                ->restrictOnUpdate()
                ->restrictOnDelete();
            $table->foreignId('parent_id')->nullable()
                ->constrained('tasks')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
            $table->string('title')->fullText();
            $table->string('status')->default(Status::TODO->value);
            $table->unsignedTinyInteger('priority')->default(1);
            $table->text('description')->nullable();
            $table->dateTime('completed_at')->nullable();
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->nullable()->useCurrentOnUpdate();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
