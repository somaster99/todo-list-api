<?php

declare(strict_types=1);

namespace Database\Factories\Task;

use Domain\Task\Enums\Status;
use Domain\Task\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Task>
 */
class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $date = fake()->dateTime;
        return [
            'status' => fake()->randomElement(Status::values()),
            'priority' => fake()->numberBetween(1, 5),
            'title' => fake()->sentence,
            'description' => fake()->text,
            'created_at' => $date,
            'updated_at' => $date
        ];
    }
}
