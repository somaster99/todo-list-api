<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use App\Http\Api\Controllers\Auth\AuthController;

Route::name('auth.')->prefix('auth')->group(function () {
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum')->name('logout');
});
