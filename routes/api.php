<?php

declare(strict_types=1);

use Domain\Task\Models\Task;
use Illuminate\Support\Facades\Route;
use App\Http\Api\Controllers\Task\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware(['api', 'auth:sanctum'])->prefix('')->name('api.')->group(function () {
    Route::name('tasks.')->group(function () {
        Route::get('tasks', [TaskController::class, 'index'])
            ->name('index')
            ->middleware('can:viewAny,' . Task::class);
        Route::post('tasks', [TaskController::class, 'store'])
            ->name('store')
            ->middleware('can:create,' . Task::class);
        Route::get('tasks/{task}', [TaskController::class, 'show'])
            ->name('show')
            ->middleware('can:view,task');
        Route::put('tasks/{task}', [TaskController::class, 'update'])
            ->name('update')
            ->middleware(
                'can:update,task'
            );
        Route::delete('tasks/{task}', [TaskController::class, 'destroy'])
            ->name('destroy')
            ->middleware('can:delete,task');

        Route::patch('/tasks/{task}/complete', [TaskController::class, 'markAsCompleted'])
            ->name('complete')
            ->middleware('can:markAsCompleted,task');
    });
});

require __DIR__ . '/auth.php';
