<?php

declare(strict_types=1);

namespace App\Http\Api\Controllers\Task;

use Domain\Task\Models\Task;
use App\Http\Controllers\Controller;
use Domain\Task\Actions\GetTasksAction;
use Domain\Task\Actions\DeleteTaskAction;
use Domain\Task\Actions\UpdateTaskAction;
use Domain\Task\Actions\CreateTaskAction;
use Domain\Task\Actions\CompleteTaskAction;
use App\Http\Api\Responses\ApiErrorResponse;
use Domain\Task\DataTransferObjects\TaskData;
use Illuminate\Contracts\Support\Responsable;
use App\Http\Api\Responses\ApiSuccessResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Api\Responses\ApiNoContentResponse;
use Domain\Task\DataTransferObjects\FilterTaskData;
use Domain\Task\DataTransferObjects\UpsertTaskData;
use Throwable;

final class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @queryParam search string Filter by title. Example: con
     * @queryParam status string Filter by status. Available: todo, done Example: done
     * @queryParam priority int Filter by ended_at.Available values: 1, 2, 3, 4, 5. Example: 1
     * @queryParam sort_by string required Field to sort by. Available values: id, created_at, completed_at, priority. Defaults to 'id'. Example: id
     * @queryParam order_by string required Field to order by. Defaults to 'ASC'. Example: ASC
     * @param FilterTaskData $filterTaskData
     * @param GetTasksAction $getTasksAction
     * @return ApiSuccessResponse
     */
    public function index(FilterTaskData $filterTaskData, GetTasksAction $getTasksAction): Responsable
    {
        return new ApiSuccessResponse(
            data: TaskData::collection($getTasksAction->execute($filterTaskData)),
            metaData: ['message' => 'Tasks list was successfully fetched.']
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @bodyParam title string required The title of task. Example: Tempora natus neque a
     * @bodyParam status string required The status of task. Example: done
     * @bodyParam description string The description of task. Example: Exercitationem illo enim et ad. Sequi qui deserunt officia occaecati et eos incidunt
     * @bodyParam priority int required The priority of task. Available values: 1, 2, 3, 4, 5. Example: 1
     * @bodyParam parent_id int The parent_id of parent task. Example: 1
     * @param UpsertTaskData $data
     * @param CreateTaskAction $createTaskAction
     * @return Responsable
     */
    public function store(UpsertTaskData $data, CreateTaskAction $createTaskAction): Responsable
    {
        try {
            return new ApiSuccessResponse(
                data: TaskData::from($createTaskAction->execute($data)),
                metaData: ['message' => 'Task was created successfully.'],
                code: Response::HTTP_CREATED
            );
        } catch (Throwable $e) {
            return new ApiErrorResponse(
                e: $e,
                message: 'An error occurred while trying to create the task.'
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Task $task
     * @return Responsable
     */
    public function show(Task $task): Responsable
    {
        $task->loadMissing('subTasks');
        return new ApiSuccessResponse(
            data: TaskData::from($task),
            metaData: ['message' => 'Task was successfully fetched.']
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @bodyParam title string required The title of task. Example: Tempora natus neque a
     * @bodyParam status string required The status of task. Example: done
     * @bodyParam description string The description of task. Example: Exercitationem illo enim et ad. Sequi qui deserunt officia occaecati et eos incidunt
     * @bodyParam priority int required The priority of task. Available values: 1, 2, 3, 4, 5. Example: 1
     * @bodyParam parent_id int The parent_id of parent task. Example 1
     * @param UpsertTaskData $data
     * @param Task $task
     * @param UpdateTaskAction $updateTaskAction
     * @return Responsable
     */
    public function update(UpsertTaskData $data, Task $task, UpdateTaskAction $updateTaskAction): Responsable
    {
        try {
            return new ApiSuccessResponse(
                data: TaskData::from($updateTaskAction->execute($task, $data)),
                metaData: ['message' => 'Task has been successfully updated.'],
                code: Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return new ApiErrorResponse(
                e: $e,
                message: 'An error occurred while trying to update the task.'
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @param DeleteTaskAction $deleteTaskAction
     * @return Responsable
     */
    public function destroy(Task $task, DeleteTaskAction $deleteTaskAction): Responsable
    {
        try {
            $deleteTaskAction->execute($task);
            return new ApiNoContentResponse();
        } catch (Throwable $e) {
            return new ApiErrorResponse(
                e: $e,
                message: 'An error occurred while trying to complete the task.'
            );
        }
    }

    /**
     * Mark as completed task.
     *
     * @param Task $task
     * @param CompleteTaskAction $completeTaskAction
     * @return Responsable
     */
    public function markAsCompleted(Task $task, CompleteTaskAction $completeTaskAction): Responsable
    {
        try {
            return new ApiSuccessResponse(
                data: TaskData::from($completeTaskAction->execute($task)),
                metaData: ['message' => 'Task was completed successfully.'],
                code: Response::HTTP_CREATED
            );
        } catch (Throwable $e) {
            return new ApiErrorResponse(
                e: $e,
                message: 'An error occurred while trying to complete the task.'
            );
        }
    }
}
