<?php

declare(strict_types=1);

namespace App\Http\Api\Controllers\Auth;

use Illuminate\Http\{
    Request,
    Response,
    JsonResponse
};
use App\Http\Controllers\Controller;
use App\Http\Api\Requests\Auth\LoginRequest;
use Illuminate\Validation\ValidationException;

final class AuthController extends Controller
{
    /**
     * Login
     *
     * @unauthenticated
     * @group Auth
     * @bodyParam email string required The email of user. Example: test@example.com
     * @bodyParam password string required The password of user. Example: password
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(LoginRequest $request): JsonResponse
    {
        if (!auth()->attempt($request->validated())) {
            throw ValidationException::withMessages([
                'email' => [trans('auth.failed')]
            ]);
        }

        $deviceName = $request->header('User-Agent');
        $token = auth()->user()->createToken($deviceName)->plainTextToken;

        return response()->json([
            'token' => $token
        ], Response::HTTP_OK);
    }

    /**
     * Logout
     *
     * @group Auth
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request): Response
    {
        $request->user()->currentAccessToken()->delete();
        return response()->noContent();
    }
}
